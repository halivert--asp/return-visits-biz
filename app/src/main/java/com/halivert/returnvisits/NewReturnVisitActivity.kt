package com.halivert.returnvisits

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.halivert.returnvisits.components.DatePickerFragment
import com.halivert.returnvisits.models.ReturnVisit
import com.halivert.returnvisits.viewmodels.ReturnVisitViewModel
import java.util.*

class NewReturnVisitActivity : AppCompatActivity() {
    private lateinit var returnVisitViewModel: ReturnVisitViewModel

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.list_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_return_visit)
        setSupportActionBar(findViewById(R.id.toolbar))

        supportActionBar?.title = getString(R.string.new_return_visit)

        returnVisitViewModel = ViewModelProvider(this).get(ReturnVisitViewModel::class.java)

        setInitialDate()

        val dateEditText = findViewById<EditText>(R.id.date)
        dateEditText.setOnClickListener {
            var calendar: Calendar = Calendar.getInstance()
            if (!TextUtils.isEmpty(dateEditText.text)) {
                val result = parseDate(dateEditText.text.toString())
                if (result != null) calendar = result
            }

            DatePickerFragment(DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                val monthString = (month + 1).toString().padStart(2, '0')
                val dayString = dayOfMonth.toString().padStart(2, '0')
                dateEditText.setText("$dayString-$monthString-$year")
            }, calendar).show(supportFragmentManager, "datePicker")
        }

        val button = findViewById<Button>(R.id.save_button)
        button.setOnClickListener {
            if (isValid()) {
                val replyIntent = Intent()
                val returnVisit = ReturnVisit(
                    0,
                    findViewById<EditText>(R.id.person_name).text.toString(),
                    findViewById<EditText>(R.id.territory_number).text.toString().toInt(),
                    findViewById<EditText>(R.id.territory_description).text.toString(),
                    findViewById<EditText>(R.id.house).text.toString(),
                    findViewById<EditText>(R.id.exterior).text.toString(),
                    parseDate(findViewById<EditText>(R.id.date).text.toString())!!.time,
                    findViewById<EditText>(R.id.description).text.toString()
                )

                returnVisitViewModel.insert(returnVisit)

                setResult(Activity.RESULT_OK, replyIntent)

                finish()
            }
        }
    }

    private fun setInitialDate() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val monthString = (calendar.get(Calendar.MONTH) + 1).toString().padStart(
            2, '0'
        )
        val dayString = calendar.get(Calendar.DAY_OF_MONTH).toString().padStart(
            2, '0'
        )

        findViewById<EditText>(R.id.date).setText("$dayString-$monthString-$year")
    }

    private fun parseDate(date: String): Calendar? {
        if (date.split('-').count() == 3) {
            val calendar = Calendar.getInstance()
            val day = date.split('-')[0].toInt()
            val month = date.split('-')[1].toInt()
            val year = date.split('-')[2].toInt()
            calendar.set(year, month - 1, day)
            return calendar
        }
        return null
    }

    private fun isValid(): Boolean {
        val error = when (true) {
            TextUtils.isEmpty(findViewById<EditText>(R.id.person_name).text) ->
                getString(R.string.missing_person_name)
            TextUtils.isEmpty(findViewById<EditText>(R.id.date).text) ->
                getString(R.string.missing_date)
            TextUtils.isEmpty(findViewById<EditText>(R.id.territory_number).text) ->
                getString(R.string.missing_territory_number)
            TextUtils.isEmpty(findViewById<EditText>(R.id.territory_description).text) ->
                getString(R.string.missing_territory_description)
            TextUtils.isEmpty(findViewById<EditText>(R.id.house).text) ->
                getString(R.string.missing_house)
            TextUtils.isEmpty(findViewById<EditText>(R.id.exterior).text) ->
                getString(R.string.missing_exterior)
            TextUtils.isEmpty(findViewById<EditText>(R.id.description).text) ->
                getString(R.string.missing_description)
            else -> null
        }

        val resultParseDate = parseDate(findViewById<EditText>(R.id.date).text.toString())

        if (resultParseDate == null) {
            Toast.makeText(
                applicationContext, getString(R.string.error_on_date), Toast.LENGTH_LONG
            ).show()
            return false
        }

        if (error != null) {
            Toast.makeText(applicationContext, error, Toast.LENGTH_LONG).show()
            return false
        }

        return true
    }

    companion object {
        const val EXTRA_REPLY = "com.halivert.returnvisits.REPLY"
    }
}
