package com.halivert.returnvisits.components

import android.content.Context
import com.halivert.returnvisits.R

class MyDate {
    companion object {
        fun getDay(context: Context, day: Int): String {
            if (day in 0..6) {
                val days = arrayOf(
                    context.getString(R.string.sunday),
                    context.getString(R.string.monday),
                    context.getString(R.string.tuesday),
                    context.getString(R.string.wednesday),
                    context.getString(R.string.thursday),
                    context.getString(R.string.friday),
                    context.getString(R.string.saturday)
                )

                return days[day]
            }
            return context.getString(R.string.error)
        }
    }
}