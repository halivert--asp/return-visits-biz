package com.halivert.returnvisits.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.halivert.returnvisits.R
import com.halivert.returnvisits.models.ReturnVisit
import java.util.*

class ReturnVisitAdapter internal constructor(
    val context: Context
) : RecyclerView.Adapter<ReturnVisitAdapter.ReturnVisitViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    private var returnVisits = emptyList<ReturnVisit>()

    inner class ReturnVisitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val personNameText: TextView = itemView.findViewById(R.id.person_name)
        val dateText: TextView = itemView.findViewById(R.id.date)
        val dayOfWeekText: TextView = itemView.findViewById(R.id.day_of_week)
        val exteriorText: TextView = itemView.findViewById(R.id.exterior)
        val houseText: TextView = itemView.findViewById(R.id.house)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReturnVisitViewHolder {
        val itemView = inflater.inflate(
            R.layout.recyclerview_returnvisit_item, parent, false
        )
        return ReturnVisitViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ReturnVisitViewHolder, position: Int) {
        val current = returnVisits[position]
        val calendar = Calendar.getInstance()
        calendar.time = current.date

        val year = calendar.get(Calendar.YEAR)
        val monthString = (calendar.get(Calendar.MONTH) + 1).toString().padStart(
            2, '0'
        )
        val dayString = calendar.get(Calendar.DAY_OF_MONTH).toString().padStart(
            2, '0'
        )

        holder.dateText.text = "$dayString-$monthString-$year"
        holder.dayOfWeekText.text = MyDate.getDay(context, calendar.get(Calendar.DAY_OF_WEEK))
        holder.personNameText.text = current.personName
        holder.exteriorText.text = current.exterior
        holder.houseText.text = current.house
    }

    internal fun setWords(returnVisits: List<ReturnVisit>) {
        this.returnVisits = returnVisits
        notifyDataSetChanged()
    }

    override fun getItemCount() = returnVisits.size
}
