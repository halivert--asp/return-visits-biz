package com.halivert.returnvisits.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.halivert.returnvisits.db.AppDatabase
import com.halivert.returnvisits.db.repositories.ReturnVisitRepository
import com.halivert.returnvisits.models.ReturnVisit
import kotlinx.coroutines.launch

class ReturnVisitViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ReturnVisitRepository

    val allReturnVisits: LiveData<List<ReturnVisit>>

    init {
        val returnVisitDao = AppDatabase.getDatabase(application).returnVistDao()
        repository = ReturnVisitRepository(returnVisitDao)
        allReturnVisits = repository.allReturnVisits
    }

    fun insert(returnVisit: ReturnVisit) = viewModelScope.launch {
        repository.insert(returnVisit)
    }

    fun delete(returnVisit: ReturnVisit) = viewModelScope.launch {
        repository.delete(returnVisit)
    }
}

