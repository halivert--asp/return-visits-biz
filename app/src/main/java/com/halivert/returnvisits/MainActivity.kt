package com.halivert.returnvisits

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.halivert.returnvisits.components.ReturnVisitAdapter
import com.halivert.returnvisits.viewmodels.ReturnVisitViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var returnVisitViewModel: ReturnVisitViewModel
    private val newReturnVisitRequestCode = 1

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.list_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val adapter = ReturnVisitAdapter(this)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        returnVisitViewModel = ViewModelProvider(this).get(ReturnVisitViewModel::class.java)

        returnVisitViewModel.allReturnVisits.observe(this, Observer { words ->
            words?.let { adapter.setWords(it) }
        })

        val fab = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewReturnVisitActivity::class.java)
            startActivityForResult(intent, newReturnVisitRequestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newReturnVisitRequestCode && resultCode == Activity.RESULT_OK) {
            Toast.makeText(
                applicationContext,
                R.string.saved,
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}
