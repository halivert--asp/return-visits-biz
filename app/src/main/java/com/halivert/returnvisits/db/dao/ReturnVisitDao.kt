package com.halivert.returnvisits.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.halivert.returnvisits.models.ReturnVisit

@Dao
interface ReturnVisitDao {
    @Query("SELECT * FROM return_visits")
    fun getAll(): LiveData<List<ReturnVisit>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(returnVisit: ReturnVisit)

    @Delete
    suspend fun delete(returnVisit: ReturnVisit)

    @Query("DELETE FROM return_visits")
    suspend fun deleteAll()
}