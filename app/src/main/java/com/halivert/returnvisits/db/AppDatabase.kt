package com.halivert.returnvisits.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.halivert.returnvisits.db.converters.DateConverter
import com.halivert.returnvisits.db.dao.ReturnVisitDao
import com.halivert.returnvisits.models.ReturnVisit

@Database(entities = [ReturnVisit::class], version = 1)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun returnVistDao(): ReturnVisitDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "return_visits_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}