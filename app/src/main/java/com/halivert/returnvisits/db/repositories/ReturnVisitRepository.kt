package com.halivert.returnvisits.db.repositories

import androidx.lifecycle.LiveData
import com.halivert.returnvisits.db.dao.ReturnVisitDao
import com.halivert.returnvisits.models.ReturnVisit

class ReturnVisitRepository(private val returnVisitDao: ReturnVisitDao) {
    val allReturnVisits: LiveData<List<ReturnVisit>> = returnVisitDao.getAll()

    suspend fun insert(returnVisit: ReturnVisit) {
        returnVisitDao.insert(returnVisit)
    }

    suspend fun delete(returnVisit: ReturnVisit) {
        returnVisitDao.delete(returnVisit)
    }

}