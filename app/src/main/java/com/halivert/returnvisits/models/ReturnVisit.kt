package com.halivert.returnvisits.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "return_visits")
data class ReturnVisit(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "person_name") val personName: String,
    @ColumnInfo(name = "territory_number") val territoryNumber: Int,
    @ColumnInfo(name = "territory_description") val territoryDescription: String,
    @ColumnInfo(name = "house") val house: String,
    @ColumnInfo(name = "exterior") val exterior: String,
    @ColumnInfo(name = "date") val date: Date,
    @ColumnInfo(name = "description") val description: String
)